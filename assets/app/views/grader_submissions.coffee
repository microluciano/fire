$ ->
  $('.fileaction').click () ->
    $this = $(this)
    subid = $this.closest('.submission').data('submission')
    filename = $this.closest('tr').data('filename')
    window.location.href += "/" + subid + "/files/" + filename + "?dl=1"

  form = $('#review-form')
  if form.length

    autosave_interval = 5000
    autosave_lastval = form.serialize()
    autosave_time = -1
    autosave_ongoing = false

    autosave = ->
      newdata = form.serialize()
      if !autosave_ongoing and newdata != autosave_lastval
        now = new Date()
        if now.getTime() - autosave_time > autosave_interval
          $.post form.data('autosave-url'), newdata, (data) ->
            if data.error
              $('#autosave_notification', form).text('Autosave failed: ' + data.error)
            else
              autosave_lastval = newdata
              autosave_time = now.getTime()
              $('#autosave_notification', form).text('Autosaved at ' + now.toLocaleTimeString())
      setTimeout autosave, autosave_interval

    autosave()
