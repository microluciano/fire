$ ->
  $('#loginform > input[name=email]').focus()
  btn = $ '#signup-btn'
  btn.click ->
    $('#signup-form-row').collapse('show')
    btn.prop('disabled', true)
    $('#loginform input').prop('disabled', true)
    $('#loginform button').prop('disabled', true)
    $('#signupform input[name=email]').focus()
  if window.location.hash == "#register"
    btn.click()
    $('#signupform input[name=email]').focus()
