$ ->
  $('.lab h2').click () -> 
    $('a', this).click()

  $('a[data-reveal]').click ->
    $this = $(this)
    $this.replaceWith("<code>" + $this.data('reveal') + "</code>")
  
  $('#modal-newgroup input[name=joincode]').keyup ->
    if $(this).val().length > 0
      $('#confirm-newgroup').text('Join group')
    else
      $('#confirm-newgroup').text('Create group')

  $('#confirm-newgroup').click ->
    $('#modal-newgroup form').submit()
