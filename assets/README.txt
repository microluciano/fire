This directory contains asset files for Fire. They are compiled by Brunch
and stored in ../fire/static, which is then served to the browser.

Brunch takes care of concatenating JavaScript and CSS, as well as minifying
and doing any conversions (e.g. from CoffeeScript or Less).

To install Brunch (and other dependencies), install nodejs and npm, and run:

  $ npm install .

from within this folder. This installs all the dependencies in the node_modules
directory, which is ignored by git. This needs only be done once on the
development machine.

Then compile the assets by running

  $ node_modules/brunch/bin/brunch build

You can also start brunch in watch mode, which will rebuild whenever some of
the assets file changes.

  $ node_modules/brunch/bin/brunch watch

Brunch is configured through the config.coffee file in this directory.

