Fire development
================

Fire is a web-application to collect and grade student labs
in electronic form. It is used and developed by the computer
science and engineering department at Chalmers University of
Technology, in Sweden

Features
--------

- Multiple labs per course
- Automatic grader assignment
- Group/individual labs

Installation
------------

Setting up for development
..........................

Create and activate a new virtual environment.

::

    cd <directory containing this file>
    pip install -e .
    mkdir data
    fireadmin -c development.ini initdb
    fireadmin -c development.ini sampledata # optional
    pserve --watch development.ini

Compiling assets (coffescript, less -> javascript, css)
.......................................................

::

    cd assets
    npm install .
    ./node_packages/brunch/bin/brunch watch


Contribute
----------

- Source Code: https://bitbucket.org/cse-fire/fire
- Issue Tracker: https://bitbucket.org/cse-fire/fire/issues
- CI: https://ci.zjyto.net/job/fire/

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: cse-fire@google-groups.com

License
-------

The project is licensed under the BSD license.

