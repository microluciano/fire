import transaction

from sqlalchemy import engine_from_config

from ..models import DBSession, Settings

def main(settings, arguments):
    """settings [<setting-name> [<new-value>]]

    Sets or retreives course-specific settings. 
    Lists all settings if no setting-name is specified
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    session = DBSession()
    
    setting_name = arguments['<setting-name>']
    new_value = arguments['<new-value>']

    with transaction.manager:
        if setting_name is None:
            ss = Settings.get_all()   # to preload all in one SQL query
            keys = Settings._defaults.keys()

            for key in keys:
                s = Settings.get(key)
                is_default = s == Settings._defaults[key]
                print('%32s = %r  %s' % (key, s, '(default)' if is_default else ''))
            return

