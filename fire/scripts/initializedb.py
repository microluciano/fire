import transaction

from sqlalchemy import engine_from_config

from . import confirm_dangerous

from ..models import (
    DBSession,
    Base,
    User
    )

def main(settings, arguments):
    """initdb

    Initializes an empty database file.
    """

    _msg = """
    This command will attempt to create tables in the database configured in the configuration.
    DO NOT perform this operation on a live database.
    ONLY perform this operation on a fresh, *empty* data directory.
    """

    confirm_dangerous(_msg)

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
