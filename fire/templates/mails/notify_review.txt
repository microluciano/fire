Hello ${submitter_name},

{% if accepted %}
Congratulations, your submission to

  ${submission.lab.title}

has been accepted by ${user.name}.
{% end %}
{% if not accepted %}
Unfortunately, your submission to

  ${submission.lab.title}

has been rejected by ${user.name}.
{% end %}
{% if len(review) %}

The grader has supplied the following feedback:

${review}
{% end %}

thanks!
Fire & the teachers of ${course_name}
