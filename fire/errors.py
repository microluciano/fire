# TODO: go through all throws and make sure they use these when appropriate
# TODO: Add a global handler to catch internal errors and log them (via a tween)

# Note on terminology: *Error are errors that are not expected to be caught (except
# by global catch-all handlers), while *Exception are logic errors to be caught
# and dealt with (even if just to show a message to the user)

class InternalFireError(Exception):
    """A base for exceptions that are not shown to the user."""
    pass

class UserException(Exception):
    """A base for exceptions that are shown to the user."""
    pass

class DataInvariantException(UserException):
    """A violation of data invariants, that cannot be enforced by model constraints."""
    pass
