import os
import errno
import uuid
import json
from datetime import datetime
import unicodedata

from . import settings
from . import errors

"""File storage and processing."""

import logging
log = logging.getLogger(__name__)

def filestore_path(request):
    return os.path.abspath(os.path.normpath(
        os.path.join(settings.get(request, 'data_dir'), 'filestore')))

def temp_path(request):
    return os.path.abspath(os.path.normpath(
        os.path.join(settings.get(request, 'data_dir'), 'tmp')))

def ensure_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def sanitize_filename(fn):
    # IE sends absolute paths as filenames, so in case there are
    # backslashes, we grab only the last part.
    if '\\' in fn:
        fn = fn.split('\\')[-1]
        assert len(fn) > 0

    fn = fn.replace('\\', '-')   # redundant because of above, but just in case
    fn = fn.replace('/', '-')
    fn = fn.replace(':', '-')
    fn = fn.replace('~', '-')

    return fn

def path_for_submission(request, submission):
    lab_str = "lab%02d" % submission.lab.id
    submitter_str = (submission.submitters[0].id 
                        if submission.lab.individual
                        else "group%02d" % submission.group.id)
    submission_str = "submission%02d" % submission.number

    return os.path.join(filestore_path(request), lab_str, submitter_str, submission_str)


class FileMetadata(object):
    def __init__(self,filepath):
        self._name  = os.path.basename(filepath)
        self._path  = filepath
        self._size  = os.path.getsize(filepath)
        self._utime = datetime.fromtimestamp(os.path.getmtime(filepath))

    @property
    def name(self): return self._name
    @property
    def size(self): return self._size
    @property
    def path(self): return self._path
    @property
    def utime(self): return self._utime


def store_file(request, submission, file_name, file_data):
    try:
        tmpdir = temp_path(request)
        ensure_dir_exists(tmpdir)
        tmp_fn = os.path.join(tmpdir, str(uuid.uuid4()))
        tmp_file = file(tmp_fn, 'wb')

        file_data.seek(0)
        while True:
            data = file_data.read(2<<16)
            if not data:
                break
            tmp_file.write(data)

        tmp_file.close()

        final_dir = path_for_submission(request, submission)
        ensure_dir_exists(final_dir)
        fn = sanitize_filename(file_name)
        final_path = os.path.join(final_dir, fn)

        if os.path.exists(final_path):
            raise errors.UserException("This file already exists!")

        os.rename(tmp_fn, final_path)
        return FileMetadata( final_path )
    except errors.UserException, e:
        raise e
    except Exception, e:
        raise errors.InternalFireError(e)

def delete_file(request, submission, filepath):
    d = path_for_submission(request, submission)
    fullpath = os.path.join(d, filepath)

    if not os.path.exists(fullpath):
        log.error("attempted delete of non-existing file %s", fullpath)
        raise InternalFireError("delete_file: File does not exist")

    ensure_dir_exists(os.path.join(d, '.deleted'))

    now = datetime.now()

    del_path = os.path.join('.deleted', now.strftime('%Y%m%d%H%M%S-') + filepath)

    os.rename(fullpath, os.path.join(d, del_path))

def files_for_submission(request, submission):
    """Returns a dict with two lists, files and deleted_files.
    The first list contains FileMetadata objects, the second dicts with the
    following keys:
     - name : The base name of the file
     - path : The path to the actual file, relative to the submission dir
     - size : The size of the file in bytes, according to the filesystem
    """
    d = path_for_submission(request, submission)

    if not os.path.isdir(d):
        return dict(files=[], deleted_files=[])

    def file_info(filepath, deleted=False):
        # OS-X uses a different normalization form than most others.
        # Normalize to NFC
        if isinstance(filepath, unicode):
            filepath = unicodedata.normalize('NFC', filepath)
        basename = os.path.basename(filepath)
        return dict(
                name = basename,
                path = basename,
                size = os.path.getsize(filepath),
                utime = datetime.fromtimestamp(os.path.getmtime(filepath))
            )

    files = [FileMetadata(os.path.join(d,p))
                for p in os.listdir(d)
                if not p.startswith('.')]

    files.sort(key=lambda info: info.name)
    
    deleted_dir = os.path.join(d, '.deleted')
    if os.path.isdir(deleted_dir):
        deletedfiles = [file_info(os.path.join(deleted_dir, p))
                for p in os.listdir(deleted_dir)
                if not p.startswith('.')]
        for info in deletedfiles:
            dt, fn = info['name'].split('-', 1)
            info['path'] = os.path.join('.deleted', info['name'])
            info['deleted'] = datetime.strptime(dt, '%Y%m%d%H%M%S')
            info['name'] = fn
        deletedfiles.sort(key=lambda info: (info['name'], info['deleted']))
    else:
        deletedfiles = []

    return dict(files=files, deleted_files=deletedfiles)

def fullpath_for_file(request, submission, filepath):
    sp = path_for_submission(request, submission)
    path = os.path.join(sp, filepath)
    return path if os.path.isfile(path) else None
