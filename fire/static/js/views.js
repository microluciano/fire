define("views/grader_lab", ['require', 'jquery'], function(require, $) {
  $(function() {
  var activetab, hash, updateDefaultRows, updateHiddenRows;
  hash = window.location.hash;
  if (hash.length) {
    activetab = $('#labtabs a[href="' + hash + '"]');
  } else {
    activetab = $("#labtabs a:first");
  }
  activetab.tab("show");
  updateDefaultRows = function() {
    return $('table.table tbody:has(tr.default-row)').each(function(idx, tb) {
      var empty;
      empty = $('tr:visible:not(.default-row)', tb).length === 0;
      return $('tr.default-row', tb).toggle(empty);
    });
  };
  updateDefaultRows();
  updateHiddenRows = function() {
    var show;
    show = $('#show-all-graders').hasClass('active');
    $('tr.other-grader').toggle(show);
    return updateDefaultRows();
  };
  $('#labtabs a').click(function() {
    return setTimeout(updateDefaultRows, 0);
  });
  return $('#show-all-graders').click(function() {
    return setTimeout(updateHiddenRows, 0);
  });
});

});
define("views/grader_main", ['require', 'jquery'], function(require, $) {
  var SubmissionsGraph;

SubmissionsGraph = (function() {
  function SubmissionsGraph(canvas) {
    this.canvas = canvas;
    this.url = $(this.canvas).data('source');
    this.colorAll = $(this.canvas).data('color-all');
    this.colorMe = $(this.canvas).data('color-me');
    this.context = this.canvas.getContext("2d");
    console.log("Drawing bars from " + this.url + " with colors " + this.colorAll + " and " + this.colorMe);
    this.refresh();
  }

  SubmissionsGraph.prototype.barWidth = 12;

  SubmissionsGraph.prototype.pxPerUnit = 5;

  SubmissionsGraph.prototype.drawBar = function(index, value) {
    return this.context.fillRect(this.canvas.width - (index + 1) * (this.barWidth + 1), this.canvas.height - value * this.pxPerUnit, this.barWidth, value * this.pxPerUnit);
  };

  SubmissionsGraph.prototype.draw = function(data) {
    var count, i, _i, _j, _len, _len1, _ref, _ref1, _results;
    this.context.fillStyle = this.colorAll;
    _ref = data.all;
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      count = _ref[i];
      this.drawBar(i, count);
    }
    this.context.fillStyle = this.colorMe;
    _ref1 = data.me;
    _results = [];
    for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
      count = _ref1[i];
      _results.push(this.drawBar(i, count));
    }
    return _results;
  };

  SubmissionsGraph.prototype.refresh = function() {
    return $.ajax({
      url: this.url,
      dataType: "json",
      success: $.proxy(this.draw, this)
    });
  };

  return SubmissionsGraph;

})();

$(function() {
  var cva, _i, _len, _ref, _results;
  console.log("abcd");
  _ref = $(".bars");
  _results = [];
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    cva = _ref[_i];
    _results.push(new SubmissionsGraph(cva));
  }
  return _results;
});

});
define("views/grader_physical_overview", ['require', 'jquery'], function(require, $) {
  $(function() {
  $('.register-button').click(function() {
    var row, submitter;
    $('tr').removeClass('info');
    $('.register-button').show().next().hide();
    row = $(this).closest('tr');
    row.addClass('info');
    $(this).hide().next().show();
    return submitter = row.data('submitter');
  });
  return $('.confirm-button').click(function() {
    var btn, form, row;
    btn = $(this);
    row = btn.closest('tr');
    form = $('#register-form');
    $('[name=submitter_id]', form).val(row.data('submitter'));
    btn.button('loading');
    return $.post(form.attr('action'), form.serialize(), function(data) {
      console.log(data);
      row.removeClass('info');
      if (data.error) {
        return btn.parent().html("<span class='text-error'>Error: " + data.error + "</span>");
      } else {
        return btn.parent().html("<span class='text-success'>Registered!</span>");
      }
    });
  });
});

});
define("views/grader_roster", ['require', 'jquery'], function(require, $) {
  $(function() {
  return $('td .icon').tooltip();
});

});
define("views/grader_submissions", ['require', 'jquery'], function(require, $) {
  $(function() {
  var autosave, autosave_interval, autosave_lastval, autosave_ongoing, autosave_time, form;
  $('.fileaction').click(function() {
    var $this, filename, subid;
    $this = $(this);
    subid = $this.closest('.submission').data('submission');
    filename = $this.closest('tr').data('filename');
    return window.location.href += "/" + subid + "/files/" + filename + "?dl=1";
  });
  form = $('#review-form');
  if (form.length) {
    autosave_interval = 5000;
    autosave_lastval = form.serialize();
    autosave_time = -1;
    autosave_ongoing = false;
    autosave = function() {
      var newdata, now;
      newdata = form.serialize();
      if (!autosave_ongoing && newdata !== autosave_lastval) {
        now = new Date();
        if (now.getTime() - autosave_time > autosave_interval) {
          $.post(form.data('autosave-url'), newdata, function(data) {
            if (data.error) {
              return $('#autosave_notification', form).text('Autosave failed: ' + data.error);
            } else {
              autosave_lastval = newdata;
              autosave_time = now.getTime();
              return $('#autosave_notification', form).text('Autosaved at ' + now.toLocaleTimeString());
            }
          });
        }
      }
      return setTimeout(autosave, autosave_interval);
    };
    return autosave();
  }
});

});
define("views/login", ['require', 'jquery'], function(require, $) {
  $(function() {
  var btn;
  $('#loginform > input[name=email]').focus();
  btn = $('#signup-btn');
  btn.click(function() {
    $('#signup-form-row').collapse('show');
    btn.prop('disabled', true);
    $('#loginform input').prop('disabled', true);
    $('#loginform button').prop('disabled', true);
    return $('#signupform input[name=email]').focus();
  });
  if (window.location.hash === "#register") {
    btn.click();
    return $('#signupform input[name=email]').focus();
  }
});

});
define("views/student_lab", ["jquery", "jquery.fileupload"], function($) {
  return $(function() {
    var activeUploads, setupFileActions;
    $('[data-toggle=tooltip]').tooltip();
    activeUploads = 0;
    $("#fileupload").fileupload({
      dataType: "json",
      add: function(e, data) {
        ++activeUploads;
        $('#submit-button').button('loading');
        return data.submit();
      },
      submit: function(e, data) {
        var tr;
        tr = $('<tr/>');
        tr.append('<td class="filename">' + data.files[0].name + '</td>');
        tr.append('<td colspan="4" class="message"><div class="progress"><div class="bar" style="width: 0%;"></div></div></td>');
        data.tableRow = tr;
        return $('#filelisting tbody').append(tr);
      },
      done: function(e, data) {
        var newrow;
        if (data.result.error) {
          return $('.message', data.tableRow).addClass('message-error').text(data.result.error);
        } else {
          newrow = $(data.result.tablerow);
          setupFileActions(newrow);
          $(data.tableRow).replaceWith(newrow);
          if (--activeUploads === 0) {
            return $('#submit-button').button('reset');
          }
        }
      },
      fail: function(e, data) {
        return $('.message', data.tableRow).addClass('message-error').text("Upload failed: " + data.jqXHR.statusText);
      },
      progress: function(e, data) {
        var bar, progress;
        progress = parseInt(data.loaded / data.total * 100, 10);
        bar = $(".progress .bar", data.tableRow).css('width', progress + '%');
        if (progress >= 95) {
          return bar.addClass("progress-striped active");
        }
      }
    });
    $('#browse-button').click(function() {
      return $('#fileupload').click();
    });
    $('.modal-cancel').click(function() {
      return $(this).closest('.modal').modal('hide');
    });
    setupFileActions = function(tr) {
      var $tr;
      $tr = $(tr);
      return $('.fileaction', tr).click(function() {
        var action, filename, mdl, subid;
        action = $(this).data('action');
        filename = $tr.data('filename');
        switch (action) {
          case "delete":
            mdl = $('#modal-confirm-delete');
            mdl.find('#confirm-delete-filename').text(filename);
            mdl.find('#confirm-delete-button').unbind('click').click(function() {
              var url;
              mdl.modal('hide');
              url = $(this).data('url');
              return $.post(url, filename, function(data) {
                var tbody;
                if (data.error) {
                  return alert(data.error);
                } else {
                  tbody = $tr.parent();
                  return $tr.slideUp(function() {
                    return $tr.remove();
                  });
                }
              });
            });
            mdl.modal();
            break;
          case "download":
            subid = $tr.closest('.submission').data('submission');
            return window.location.href += "/submissions/" + subid + "/files/" + filename + "?dl=1";
        }
      });
    };
    $('.filelisting-5col tr').each(function(idx, tr) {
      return setupFileActions(tr);
    });
    return $('#submit-button').click(function() {
      var mdl, p;
      mdl = $('#modal-confirm-submit');
      p = $('#physical_submission');
      if (p.length && p.is(':checked')) {
        $('input[name=physical]').val('yes');
      }
      return mdl.modal();
    });
  });
});
define("views/student_main", ['require', 'jquery'], function(require, $) {
  $(function() {
  $('.lab h2').click(function() {
    return $('a', this).click();
  });
  $('a[data-reveal]').click(function() {
    var $this;
    $this = $(this);
    return $this.replaceWith("<code>" + $this.data('reveal') + "</code>");
  });
  $('#modal-newgroup input[name=joincode]').keyup(function() {
    if ($(this).val().length > 0) {
      return $('#confirm-newgroup').text('Join group');
    } else {
      return $('#confirm-newgroup').text('Create group');
    }
  });
  return $('#confirm-newgroup').click(function() {
    return $('#modal-newgroup form').submit();
  });
});

});
