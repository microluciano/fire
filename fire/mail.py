import os

from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message

from genshi.template import TemplateLoader
from genshi.template.text import NewTextTemplate
from pyramid.asset import abspath_from_asset_spec

from . import templating, settings
from .filestore import ensure_dir_exists

import logging
log = logging.getLogger(__name__)

def sendmail(request, recipients, subject, template, context=None):

    loader = TemplateLoader()
    path = abspath_from_asset_spec(template)
    tmpl = loader.load(path, cls=NewTextTemplate)
    body = tmpl.generate(**(context or {})).render()

    subject = '[fire:%s] %s' % (settings.get(request, 'course_slug'), subject)

    mailer = get_mailer(request)
    message = Message(subject = subject,
                      recipients = recipients,
                      body = body)

    # Create the maildir if it does not exist
    for f in ('tmp','new','cur'):
        ensure_dir_exists(os.path.join(request.registry.settings['mail.queue_path'], f))

    mailer.send_to_queue(message)
