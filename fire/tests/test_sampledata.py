""" Tests for the 'sampledata' command """

from . import DBTestCase
from fire.scripts.sampledata import create_sample_data

class SmokeTest(DBTestCase):
    """ Simply create the sample data and expect no exception """

    def test(self):
        create_sample_data(self.session)
