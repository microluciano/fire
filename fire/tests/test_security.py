from unittest import TestCase
from nose.tools import eq_, ok_

from ..security import (hash_pwd, verify_pwd)


class TestHashing(TestCase):

    def test_hash_pwd(self):
        h1 = hash_pwd('hunter2')
        h2 = hash_pwd('hunter2')
        h3 = hash_pwd('anotherone')

        assert h1 != h2
        assert h2 != h3

    def test_verify_pwd(self):
        for pw in ('hunter2', 'asdf', 'f;aefi153{?135?%33'):
            h = hash_pwd(pw)
            assert verify_pwd(pw, h)

        precomp_pwd = 'correcthorsebatterystaple'
        precomp_hash = '3ecd988ccd9248f4$sha512$65536$42f4b6a8a5a96779a497c3e19244ec121630fd2351455f1d5680c6439c3a72a6a7d5c83d3a9cdc42ac040c656f153792076002842c2115884082edc405b1ca20'
        assert verify_pwd(precomp_pwd, precomp_hash)

        with self.assertRaises(ValueError):
            verify_pwd('x', 'notvalid')
        
        with self.assertRaises(ValueError):
            # The rounds is not an integer here
            verify_pwd('', '3ecd988ccd9248f4$sha512$foo$42f4b6a8a5a96779a497c3e19244ec121630fd2351455f1d5680c6439c3a72a6a7d5c83d3a9cdc42ac040c656f153792076002842c2115884082edc405b1ca20')

        with self.assertRaises(ValueError):
            # Unknown hashing alg
            verify_pwd('', '3ecd988ccd9248f4$fancyhash$65536$42f4b6a8a5a96779a497c3e19244ec121630fd2351455f1d5680c6439c3a72a6a7d5c83d3a9cdc42ac040c656f153792076002842c2115884082edc405b1ca20')

