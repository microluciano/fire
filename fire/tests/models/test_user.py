from .. import DBTestCase

from fire.models import Student, EmailVerification, DBSession

class TestCreateStudent(DBTestCase):
    """ Test the Student.create method """

    def setUp(self):
        super(self.__class__, self).setUp()
        Student.create(
                'mr.reese@alibrary.us',
                first_name = 'John', last_name = 'Reese',
                id_number = '0000000000' )

    def test_created(self):
        """ Test student creation with good parameters """
        student = DBSession.query(Student).get('mr.reese@alibrary.us')
        self.assertIsNotNone(student)

    def test_first_name(self):
        """ it should have first name John """
        student = DBSession.query(Student).get('mr.reese@alibrary.us')
        self.assertEquals(student.first_name, 'John')



