from .. import DBTestCase

from fire.models import EmailVerification, Student

class TestCreateEmailVerification(DBTestCase):
    def test_create(self):
        ev = EmailVerification.create( self.session, email = 'test@test.com' )
        self.assertEquals('test@test.com',  ev.email )

    def test_default_token(self):
        """ Test that a 16 char long default verification token is created """
        ev = EmailVerification.create( self.session, email = 'test@test.com' )
        self.assertEquals(16, len(ev.token))

class TestCheckToken(DBTestCase):
    def setUp(self):
        super(TestCheckToken, self).setUp()
        EmailVerification.create( self.session,
                email = 'test@test.com',
                token = 'aaaabbbbccccdddd' )

    def test_valid(self):
        """ When passed a valid token, the method should return the associated
        email address"""
        self.assertEquals(
                'test@test.com',
                EmailVerification.check('aaaabbbbccccdddd') )

    def test_invalid(self):
        """ When passed an invalid token, the method should raise a ValueError
        Exception """
        with self.assertRaises( ValueError ):
            EmailVerification.check( '0000000000000000' )

class TestGetUnverified(DBTestCase):
    """ The get_unverified method is supposed to return all the emails which
    have not yet been verified. Here verified means that there is a user
    account with that email """

    def test_return_all(self):
        """ With only one token in the database and no user, it returns
        this token """
        ev = EmailVerification.create( email = 'test@test.com' )
        ret = EmailVerification.get_unverified()
        self.assertEquals(ret, [ev])

    def test_exclude_account(self):
        """ Test that it does exclude verified emails """
        ev = EmailVerification.create( email = 'test@test.com' )
        EmailVerification.create( email = 'user@test.com' )
        Student.create('John Doe <user@test.com>')
        self.assertEquals(EmailVerification.get_unverified(), [ev])

