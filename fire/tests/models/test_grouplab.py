from unittest import TestCase
from .. import DBTestCase


from fire.models import Lab, GroupLab

class TestCreateGroupLab(DBTestCase):

    def setUp(self):
        super(TestCreateGroupLab,self).setUp()
        self.session.add(GroupLab())
        self.lab = self.session.query(GroupLab).get(1)

    def test_GroupLab_is_Lab(self):
        """ Test that a GroupLab is also a Lab """
        self.assertIsInstance(self.lab, Lab)

    def test_default_min_members(self):
        """ Check the value of the defaulst min and max numbers """
        self.assertEqual(1, self.lab.min_members)

    def test_default_min_members(self):
        """ Check the existence of the defaulst max numbers """
        self.assertIsNotNone(self.lab.min_members)

class TestInitGroupLab(TestCase):
    """ Test init parameters """

    def test_min_members(self):
        lab = GroupLab( min_members = 2 )
        self.assertEquals(2, lab.min_members)

    def test_max(self):
        lab = GroupLab( max_members = 8 )
        self.assertEquals(8, lab.max_members)


class TestMinMaxChecks(DBTestCase):
    """ Test that the right checks are performed on the min and max members
    values.
    """

    def test_min_greater_than_0(self):
        """ min should be greater than 0 """
        from sqlalchemy.exc import IntegrityError
        with self.assertRaises(IntegrityError):
          self.session.add( GroupLab( min_members = 0 ) )
          self.session.flush()

    def test_max_greater_than_1(self):
        """ min should be greater than 0 """
        from sqlalchemy.exc import IntegrityError
        with self.assertRaises(IntegrityError):
          self.session.add( GroupLab( max_members = 1 ) )
          self.session.flush()

    def test_max_greater_than_min(self):
        """ creating a group lab with max_members < min_members should
        fail """
        from sqlalchemy.exc import IntegrityError
        with self.assertRaises( IntegrityError ):
            self.session.add( GroupLab( min_members = 4, max_members = 2 ) )
            self.session.flush()
