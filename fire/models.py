import re
import random
from datetime import datetime, timedelta
import random
from email.utils import parseaddr

from pyramid.security import Allow, Deny, ALL_PERMISSIONS, DENY_ALL
from pyramid.httpexceptions import HTTPNotFound

from sqlalchemy.schema import Table, Column, ForeignKey, CheckConstraint
from sqlalchemy.types import (
    SchemaType, TypeDecorator, Integer, DateTime, Text,  Enum, Boolean,
    PickleType, String )
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.engine import Engine
from sqlalchemy import event

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    object_session
    )

from zope.sqlalchemy import ZopeTransactionExtension

from .security import (hash_pwd, verify_pwd, random_hex_string)
from .errors import UserException, DataInvariantException, InternalFireError

import logging
log = logging.getLogger(__name__)

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension(), expire_on_commit=False))

# ENABLE FOREIGN KEY CONSTRAINTS
# SQLite supports FOREIGN KEY syntax when emitting CREATE statements for tables,
# however by default these constraints have no effect on the operation of the
# table. Need to turn it on for each connection!
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
      cursor = dbapi_connection.cursor()
      cursor.execute("PRAGMA foreign_keys=ON")
      cursor.close()


def includeme(config):
    from sqlalchemy import engine_from_config
    engine = engine_from_config(config.registry.settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

def model_resource(model, match=None, postparam=None):
    """Creates a resource factory, suitable for passing to Pyramid's
    add_route, that returns a model class (or 404 if not found).
    The id for the model class is taken from the URL routing matchdict
    if the match parameter is used, or from the POST data if the postaparm
    is given.
   
    Using such a factory makes Pyramid pass the model instance as the
    context parameter to the view, after consulting the instance's
    __acl__ property to see if the current user has permission to the object.
    """
    # We kind of rely on that SQLite doesn't care about types. A different db
    # would mean here we need to add some type conversion as well.
    if match:
        def model_resource_factory(request):
            return model.get_or_404(request.matchdict[match])
    elif postparam:
        def model_resource_factory(request):
            return model.get_or_404(request.POST.getone(postparam))
    else:
        raise ValueError("Must specify either a name from matchdict or request.POST to use as an id")
    return model_resource_factory


class Base(object):
    """A declarative base for SQLAlchemy models."""

    @classmethod
    def get(cls, key):
        return DBSession().query(cls).get(key)

    @classmethod
    def get_or_404(cls, key):
        obj = cls.get(key)
        if obj is None:
            raise HTTPNotFound("No such %s : %s" % (cls.__name__, str(key)))
        return obj

    @classmethod
    def get_all(cls, *args, **kwargs):
        return DBSession().query(cls).filter(*args, **kwargs).all()

    @classmethod
    def query(cls):
        return DBSession().query(cls)

Base = declarative_base(cls=Base)


# From Zzzeek's Enum recipe {{{
# http://techspot.zzzeek.org/2011/01/14/the-enum-recipe/

class EnumSymbol(object):
    """Define a fixed symbol tied to a parent class."""

    def __init__(self, cls_, name, value, description):
        self.cls_ = cls_
        self.name = name
        self.value = value
        self.description = description

    def __reduce__(self):
        """Allow unpickling to return the symbol 
        linked to the DeclEnum class."""
        return getattr, (self.cls_, self.name)

    def __iter__(self):
        return iter([self.value, self.description])

    def __repr__(self):
        return "<%s>" % self.name

class EnumMeta(type):
    """Generate new DeclEnum classes."""

    def __init__(cls, classname, bases, dict_):
        cls._reg = reg = cls._reg.copy()
        for k, v in dict_.items():
            if isinstance(v, tuple):
                sym = reg[v[0]] = EnumSymbol(cls, k, *v)
                setattr(cls, k, sym)
        return type.__init__(cls, classname, bases, dict_)

    def __iter__(cls):
        return iter(cls._reg.values())

class DeclEnum(object):
    """Declarative enumeration."""

    __metaclass__ = EnumMeta
    _reg = {}

    @classmethod
    def from_string(cls, value):
        try:
            return cls._reg[value]
        except KeyError:
            raise ValueError(
                    "Invalid value for %r: %r" % 
                    (cls.__name__, value)
                )

    @classmethod
    def values(cls):
        return cls._reg.keys()

    @classmethod
    def db_type(cls):
        return DeclEnumType(cls)

class DeclEnumType(SchemaType, TypeDecorator):
    def __init__(self, enum):
        self.enum = enum
        self.impl = Enum(
                        *enum.values(), 
                        name="ck%s" % re.sub(
                                    '([A-Z])', 
                                    lambda m:"_" + m.group(1).lower(), 
                                    enum.__name__)
                    )

    def _set_table(self, table, column):
        self.impl._set_table(table, column)

    def copy(self):
        return DeclEnumType(self.enum)

    def process_bind_param(self, value, dialect):
        if value is None:
            return None
        return value.value

    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return self.enum.from_string(value.strip())

# End Enum recipe }}}

class Settings(Base):
    """Course wide settings.
    """
    __tablename__ = 'settings'

    id = Column(Text, primary_key = True)
    value = Column(PickleType)

    _defaults = dict(
        course_name = 'Default course',   # Human friendly name of the course
        )

    _cache = dict()

    # This class deviates from base, as other code should not
    # deal directly with instances
    @classmethod
    def get(cls, key):
        if key not in cls._defaults:
            raise InternalFireError("Invalid setting name")
        if key in cls._cache:
            return cls._cache[key]
        obj = DBSession().query(cls).get(key)
        if obj is None:
            return cls._defaults[key]
        else:
            cls._cache[key] = obj.value
            return obj.value
    
    @classmethod
    def set(cls, key, value):
        if key not in cls._defaults:
            raise InternalFireError("Invalid setting name")
        cls._cache.pop(key, None)
        obj = DBSession().query(cls).get(key)
        if obj is None:
            obj = cls(id=key)
            DBSession().add(obj)
        obj.value = value


class CourseCode(Base):
    __tablename__ = "coursecodes"
    code = Column(String(10), CheckConstraint("code <> ''"), primary_key = True )

class SubmitterMixin(object):
    """A mixin for Users and Groups. Includes common functionality for
    submitters."""

    # TODO: perhaps these are better kept in the Lab class. Refactor?
    
    def submissions_for_lab(self, lab):
        """Returns all submissions for a given lab for this user or group."""
        lab_subs = [sub for sub in self.submissions if sub.lab == lab]
        lab_subs.sort(key=lambda s: s.number)
        return lab_subs

    def get_pending_submission(self, lab):
        """Returns the pending submission for a lab, if one exists, None otherwise."""
        subs = [sub for sub in self.submissions_for_lab(lab)
                if sub.status == Submission.Statuses.submitted]
        if len(subs) > 0:
            assert len(subs) == 1  # Should be maintained always
            return subs[0]
        else:
            return None

    def open_new_submission(self, lab):
        """Creates a new submission for lab for uploading files etc.
        Returns the current submission if an unsubmitted one already
        exists. The lab must be a group lab."""
        subs = self.submissions_for_lab(lab)
        if len(subs) and any(s.active for s in subs):
            return subs[-1]
        else:
            return Submission(lab, self)  # Will raise if self doesnt match lab.individual

class EmailVerification(Base):
    """ Allow to verify an email when users register"""
    __tablename__ = 'emailverifications'
    email = Column(Text)
    token = Column(
        Text,
        primary_key = True,
        default = lambda:random_hex_string(16))
    cdate = Column(
        DateTime,
        default = datetime.now)

    @staticmethod
    def create( dbsession = DBSession, **kwargs ):
        verif = EmailVerification( **kwargs )
        dbsession.add(verif)
        dbsession.flush()
        return verif

    @classmethod
    def check(cls, token):
        """ Checks a token: returns the email address for a valid token
        or raise an exception """
        verif = DBSession.query(cls).get(token)
        if verif is None:
            raise ValueError('Invalid verification token')
        else:
          return verif.email

    @classmethod
    def get_unverified(cls):
        """ Returns rows that don't correspond to a user account """
        verified = DBSession().query(User.email).subquery()
        return DBSession.query(cls).filter(~cls.email.in_(verified)).all()


class User(Base, SubmitterMixin):
    """A user in the system (any kind; students, teachers, admins)
    """
    __tablename__ = 'users'

    class Roles(DeclEnum):
        student = 'student', 'Student'
        grader  = 'grader' , 'Grader'
        admin   = 'admin'  , 'Course Admin'

    email      = Column(Text, primary_key = True)
    _pw_hash   = Column('pw_hash', Text) 
    first_name = Column(Text)
    last_name  = Column(Text)
    last_login = Column(DateTime)                        #: date last logged in
    role       = Column(String(50), nullable = False)


    __mapper_args__ = { 'polymorphic_on': role }

    @property
    def id(self):
        """ ID property, created for backward compatibility"""
        return self.email

    def __init__(self, id_, **kwargs):
        """
        Initialize a User using a valid mail recipient.
        For instance, ``User("test@test.com")`` will set the user email to
        "test@test.com" but ``User("Bob Bobsson <bob@test.com>")`` will also set the
        user first and last names.
        """
        super(User, self).__init__(**kwargs)
        name, address = parseaddr(id_)
        self.email = address
        try:
          names = name.split(' ', 1)
          self.first_name = self.first_name or names.pop(0)
          self.last_name  = self.last_name or names.pop(0)
        except IndexError:
          pass

    @classmethod
    def create(cls, *args, **kargs):
        u = cls(*args, **kargs)
        DBSession.add(u)
        DBSession.flush()

    @property
    def __acl__(self):
        return [ (Allow, 'role:admin', ALL_PERMISSIONS) ]

    @hybrid_property
    def password(self):
        """Always an empty string, only used for setting a password."""
        # Note: As a convention, do not handle _pw_hash outside this class.
        # It contains a salted hash, so leaking it opens up the possibility
        # of rainbow attacks.
        return ""

    @password.setter
    def password(self, value):
        """Updates the stored hashed password, actual value is not stored."""
        self._pw_hash = hash_pwd(value)

    @classmethod
    def check_password(cls, userid, pwd):
        """Returns true if ``pwd`` matches the stored hash of the users password.
        Returns false if the user is not verified.
        """
        u = cls.get(userid)
        log.debug('checking_password: for %r', u)
        if u is not None:
            return verify_pwd(pwd, u._pw_hash)
        return False

    @property
    def name(self):
        if self.first_name and self.last_name:
          return "%s %s" % (self.first_name, self.last_name)
        elif self.first_name:
          return self.first_name
        elif self.last_name:
          return self.last_name
        else:
          return None

    @property
    def safe_info(self):
        """Returns a dictionary of information about the user that is safe to display."""
        info = dict()
        for prop in ['id', 'email', 'name', 'id_number', 'course']:
            info[prop] = getattr(self, prop, None)
        return info

    @property
    def active_membership(self):
        ag = [m for m in self.memberships if m.active]
        assert len(ag) <= 1, "Multiple active groups for user."
        if len(ag):
            return ag[0]
        else:
            return None

    def __repr__(self):
        return "<User %s>" % self.email

    def __unicode__( self ):
        return u"%s %s <%s>" % (self.first_name, self.last_name, self.email)


class Student(User):
    __tablename__ = 'students'
    __mapper_args__ = { 'polymorphic_identity':'student' }
    email = Column( Text, ForeignKey('users.email'), primary_key=True )
    id_number  = Column(Text, unique=True)                        #: student id number
    course     = Column(String(10), ForeignKey('coursecodes.code', ondelete = 'restrict'))
    group_id = Column(Integer, ForeignKey('groups.id'))
    signup     = Column(DateTime, default=datetime.now)  #: date signed up

    __acl__ = [ (Allow, 'role:grader', 'view'),
                (Allow, 'role:grader', 'edit'),
                (Allow, 'role:grader', 'impersonate'),
                (Allow, 'role:admin', ALL_PERMISSIONS),
              ]

class Grader(User):
    __tablename__ = 'graders'
    __mapper_args__ = { 'polymorphic_identity':'grader' }
    email = Column( Text, ForeignKey('users.email'), primary_key=True )

    __acl__ = [ (Allow, 'role:admin', ALL_PERMISSIONS) ]

_joincodeletters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'
def mkjoincode():
    groups = Group.get_all()
    existing_codes = set(g.joincode for g in groups)
    for i in xrange(512):
        code = ''.join(random.choice(_joincodeletters) for i in xrange(6))
        if code not in existing_codes:
            return code
    # Probability of reaching here: 1/(Infinity - 1)
    raise InternalFireError("Failed to generate a joincode.")

class Group(Base, SubmitterMixin):
    __tablename__ = 'groups'

    id       = Column(Integer, primary_key=True)
    created  = Column(DateTime, default=datetime.now)
    joincode = Column(Text, nullable=False, default=mkjoincode)

    members = relationship("Student", backref="group")

    def __init__(self, owner):
        self.members.append(owner)

    # For convenience, read-only
    #active_members = relationship(User, secondary='group_memberships',
    #        secondaryjoin = and_(Membership.user_id == User.id, Membership.active == True),
    #        viewonly=True)

    @property
    def active_members(self):
        return [ u for u in self.members ]

    def _refresh(self):
        s = object_session(self)
        if s is not None:
            s.refresh(self)

    def add_user(self, user):
        if user in self.active_members:
            return
        self.members.append(user)
        # Add the user to any unsubmitted submissions
        for sub in self.submissions:
            if sub.status is Submission.Statuses.new and user not in sub.submitters:
                sub.submitters.append(user)

    def remove_user(self, user):
        if user not in self.active_members:
            raise ValueError("Group.remove_user called with a non-member.")
        self.members.remove(user)
        # Remove the user from any open (i.e unsubmitted) submissions
        for sub in self.submissions:
            if sub.status is Submission.Statuses.new and user in sub.submitters:
                sub.submitters.remove(user)
        #self._refresh()  # To update active_members property

    def __repr__(self):
        return "<Group %d>" % self.id

    def __unicode__(self):
        return u"Group %d" % self.id


class Lab(Base):
    __tablename__ = 'labs'

    id = Column(Integer, primary_key=True)
    title = Column(Text)
    slug = Column(Text)
    description = Column(Text)
    created = Column(DateTime, default=datetime.now)
    order = Column(Integer, default=0)
    active = Column(Boolean, default=True)
    individual = Column(Boolean, nullable=False)
    allow_physical_submissions = Column(Boolean, default=False, nullable=False)
    default_deadline1 = Column(DateTime)
    default_deadline2 = Column(DateTime)

    __mapper_args__ = {
        'polymorphic_on': individual,
        'polymorphic_identity': True }

    @property
    def grader_assignment(self):
        """ This should return the same information as self.grader but instead
        of returning a list of GraderAssignments, it returns a list of pairs
        where the first element is an integer representing the weight and
        the second is the grader id (email).

        This property exists for backward compatibility reasons.
        """
        return [ (a.weight, a.grader.email ) for a in self.graders]

    @classmethod
    def get_active(cls):
        return cls.query().filter(Lab.active == True).order_by(Lab.order).all()

    def summary_for_user(self, user):
        """Returns a dict containing the keys:
           - status : The general submission status. Ignores cancelled
               submissions, and new submissions unless it's the only one.
           - deciding_submission : the submission that decided the status
               above, e.g. the latest accepted or rejected submission,
               or the latest non-cancelled submission if none are accepted
               or rejected.
           - deadline1 : The effective deadline1  (may be overridden)
           - deadline2 : The effective deadline2
           - deadline1_override : True iif deadline1 is not the default deadline.
           - deadline2_override : True iif deadline2 is not the default deadline.
        """
        subs = user.submissions_for_lab(self)

        deadline1_override = False
        deadline2_override = False
        
        if len(subs):
            # Deadline overrides always come from the latest submission,
            # regardless of their status.
            if subs[-1].deadline1_override is not None:
                deadline1 = subs[-1].deadline1_override
                deadline1_override = True
            else:
                deadline1 = self.default_deadline1
            
            if subs[-1].deadline2_override is not None:
                deadline2 = subs[-1].deadline2_override
                deadline2_override = True
            else:
                deadline2 = self.default_deadline2

            accepted_or_rejected = [s for s in subs
                    if s.status in (Submission.Statuses.accepted, 
                                    Submission.Statuses.rejected,
                                    Submission.Statuses.submitted)]
            if len(accepted_or_rejected):
                deciding_submission = accepted_or_rejected[-1]
                status = deciding_submission.status
            else:
                not_cancelled = [s for s in subs if s.status != Submission.Statuses.cancelled]
                if len(not_cancelled):
                    deciding_submission = not_cancelled[-1]
                    status = deciding_submission.status  # Will be submitted or new
                else:
                    status = Submission.Statsuses.new
                    deciding_submission = None
        else:
            deadline1 = self.default_deadline1
            deadline2 = self.default_deadline2
            status = Submission.Statuses.new
            deciding_submission = None

        # Single deadline-labs have only a final deadline:
        if deadline1 is None:
            deadline1 = deadline2

        return dict(deadline1 = deadline1,
                    deadline2 = deadline2,
                    deadline1_override = deadline1_override,
                    deadline2_override = deadline2_override,
                    status = status,
                    deciding_submission = deciding_submission)

    def __repr__(self):
        return '<Lab: %s>' % self.title

class GroupLab(Lab):
    __tablename__ = 'grouplabs'

    id = Column( Integer, ForeignKey('labs.id'), primary_key=True )
    max_members = Column(
            Integer,
            CheckConstraint( 'max_members > 1' ),
            CheckConstraint( 'max_members >= min_members' ),
            default=2)
    min_members = Column(Integer, CheckConstraint('min_members > 0'), default=1)

    __mapper_args__ = { 'polymorphic_identity': False }

class GraderAssignment(Base):
    """ Represent the fact that a gradder is assigned to a lab. The weight
    is used when a new submission comes in to randomly assign a grader to
    that submission, it allow precise control of the submission distribution.
    """
    __tablename__ = 'grader_assignments'

    lab_id = Column( Integer, ForeignKey('labs.id'), primary_key = True )
    grader_id = Column( Text, ForeignKey('graders.email'), primary_key = True )
    weight = Column( Integer, CheckConstraint( 'weight > 0' ),
                     nullable = False, default = 1 )

    lab = relationship('Lab', backref='graders' )
    grader = relationship('Grader')

submitters_table = Table('submitters', Base.metadata,
    Column('submission_id', Integer, ForeignKey('submissions.id')),
    Column('user_id', Text, ForeignKey('users.email'))
)

class Submission(Base):
    __tablename__ = 'submissions'

    class Statuses(DeclEnum):
        new       = 'new'       , 'Not submitted'
        submitted = 'submitted' , 'Submitted'
        cancelled = 'cancelled' , 'Withdrawn'
        accepted  = 'accepted'  , 'Accepted'
        rejected  = 'rejected'  , 'Rejected'

    # List of "active" statuses. See the active property below for explanation.
    _active_statuses = ( Statuses.new,
                         Statuses.submitted,
                         Statuses.accepted )

    id = Column(Integer, primary_key=True)
    lab_id = Column(Integer, ForeignKey('labs.id'), nullable=False)
    group_id = Column(Integer, ForeignKey('groups.id'))
    number = Column(Integer, nullable=False)
    _status = Column(Statuses.db_type(), nullable=False)
    deadline1_override = Column(DateTime)
    deadline2_override = Column(DateTime)
    grader_id = Column(Text, ForeignKey('graders.email'))

    submitted_date = Column(DateTime)  # Date of submission
    reviewed_date = Column(DateTime)   # Date of approval / rejection

    physical_submission = Column(Boolean)
    physical_submission_received_by_id = Column(Text, ForeignKey('graders.email'))
    physical_submission_received_date = Column(DateTime)

    grade = Column(Text)
    note = Column(Text)
    review = Column(Text)

    # Same as above, used for autosaving
    grade_draft = Column(Text)
    note_draft = Column(Text)
    review_draft = Column(Text)

    
    lab = relationship(Lab, backref='submissions')
    group = relationship(Group, backref='submissions', lazy=False)
    grader = relationship(Grader, backref='grader_submissions',
            primaryjoin=grader_id==Grader.email)
    physical_submission_received_by = relationship(Grader,
            primaryjoin=physical_submission_received_by_id==Grader.email)
    submitters = relationship(Student, secondary=submitters_table, backref='submissions')

    def __init__(self, lab, group_or_submitter):

        if lab.individual and not isinstance(group_or_submitter, Student):
            raise InternalFireError("Tried creating a group submission on an individual lab.")
        elif not lab.individual and not isinstance(group_or_submitter, Group):
          if group_or_submitter.group is not None:
            group_or_submitter = group_or_submitter.group
          else:
            raise InternalFireError("Tried creating an individual submission on a group lab.")

        # These non-null properties must be set before .lab and .group,
        # to avoid db integrity errors when the latter generate inserts (to obtain an id)
        # Note that .status is set below to trigger a check for double-submissions, so
        # it is important to set a *non-active* status here.
        self._status = Submission.Statuses.cancelled
        self.number = len(group_or_submitter.submissions_for_lab(lab)) + 1

        if self.number > 1:
            prevsub = group_or_submitter.submissions_for_lab(lab)[-1]

            # If the last submission for this group/user had a deadline
            # override, then copy it to the new one.
            self.deadline1_override = prevsub.deadline1_override  # may just be None
            self.deadline2_override = prevsub.deadline2_override

            # Copy the grader from the last submission
            self.grader = prevsub.grader
        else:
            # Brand new submission, assign a grader
            weights = lab.grader_assignment
            if len(weights) == 0:
                raise InternalFireError("Misconfiguration: No grader weights are set on lab %d." % lab.id)
            gs = []
            for (weight, grader) in weights:
                gs.extend(weight * [grader])
            choice = random.choice(gs)
            grader = Grader.get(choice)
            if grader is None:
                raise InternalFireError("Misconfiguration: Grader %s does not exist or isn't a grader." % choice)
            self.grader = grader


        self.lab = lab
        if lab.individual:
            assert isinstance(group_or_submitter, Student)
            self.submitters = [group_or_submitter]
            self.group = None
        else:
            assert isinstance(group_or_submitter, Group)
            self.group = group_or_submitter
            self.submitters = group_or_submitter.active_members[:]

        # Now set the status correctly through the setter, to catch the case where 
        # a student already has an active submission on this lab.
        self.status = Submission.Statuses.new

    @property
    def active(self):
        """An active submission is or will may be counted towards a grade.
        Thus, each student can only have one active submission per lab."""
        return self._status in Submission._active_statuses

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, newstatus):
        oldstatus = self._status
        if ( oldstatus not in Submission._active_statuses 
                and newstatus in Submission._active_statuses):
            # Need to make sure none of the submitting students have
            # other active submissions on this lab.
            all_active_submissions = [s for s in self.lab.submissions if s.active and s is not self]
            users_with_active_subs = [user.email for sub in all_active_submissions
                                              for user in sub.submitters]
            overlap = set(u.email for u in self.submitters) & set(users_with_active_subs)
            if len(overlap):
                raise DataInvariantException("The following users already have submissions to this lab: %s" 
                        % ','.join(map(str, overlap)))
        if newstatus is Submission.Statuses.submitted:
            self.submitted_date = datetime.now()
        elif newstatus in (Submission.Statuses.rejected, Submission.Statuses.accepted):
            self.reviewed_date = datetime.now()
        elif newstatus is Submission.Statuses.cancelled:
            if oldstatus in [Submission.Statuses.rejected, Submission.Statuses.accepted]:
                raise ValueError("Impossible to withdraw a reviewed submission")
        self._status = newstatus
        # TODO: add logging

    def submit(self):
        """
        This method is a shortcut to set the submission status to submitted
        """
        self.status = self.Statuses.submitted

    def withdraw(self):
        """
        This method is a shorctut to set the submission status to cancelled
        """
        self.status = self.Statuses.cancelled

    def post_review(self, accepted, grade, note, review):
        self.status = Submission.Statuses.accepted if accepted else Submission.Statuses.rejected
        self.reviewed_date = datetime.now()

        self.grade = self.grade_draft = grade
        self.note = self.note_draft = note
        self.review = self.review_draft = review


class FileCap(Base):
    __tablename__ = 'filecaps'

    id = Column(Text, primary_key=True)
    is_archive = Column(Boolean, nullable=False, default=False)
    filename = Column(Text, nullable=False)
    contents = Column(PickleType, nullable=False)
    issued = Column(DateTime, nullable=False, default=datetime.now)
    expires = Column(DateTime, nullable=False)
    ip_address = Column(Text)

    def __init__(self, ttl=60):
        self.id = random_hex_string(32)
        self.issued = datetime.now()
        self.expires = self.issued + timedelta(seconds=ttl)

    @classmethod
    def issue_single(cls, path, name, ttl=60):
        """Issues a filecap for a single file. The path to the file
        is stored in the contents field."""
        fc = cls(ttl)
        fc.is_arvhive = False
        fc.filename = name
        fc.contents = path
        DBSession().add(fc)
        return fc

    @classmethod
    def issue_archive(cls, paths, name, ttl=60):
        """Issues a filecap for an archive of files (created on download).
        The contents field will be set to a list of pairs, each containing
        a full fileystem path to a file as well as the path of that file
        within the archive."""
        fc = cls(ttl)
        fc.is_archive = True
        fc.filename = name
        fc.contents = paths[:]
        DBSession().add(fc)
        return fc
