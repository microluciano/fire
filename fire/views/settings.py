import colander
from pyramid.view import view_config
from deform import Form
from deform import ValidationFailure
from pyramid.httpexceptions import HTTPFound

from ..models import DBSession,Lab
from ..forms import genshi_deform_renderer, DateTimeInput, DateTime

class LabSettings(colander.MappingSchema):
    default_deadline1 = colander.SchemaNode(DateTime(),
                                            widget=DateTimeInput(),
                                            missing=None)
    default_deadline2 = colander.SchemaNode(DateTime(),
                                            widget=DateTimeInput(),
                                            missing=None)

class LabSettingsView(object):
    def __init__(self, request):
        self.request = request

    @view_config( route_name='lab_settings',
                  renderer='fire:templates/lab_settings.html',
                  permission='has_role:grader' )
    def site_view(self):
        lab = Lab.get_or_404(int(self.request.matchdict['lab']))
        schema = LabSettings()
        myform = Form(schema, buttons=('submit',),
                      renderer=genshi_deform_renderer,
                      csrf_token=self.request.session.get_csrf_token() )
        if 'submit' in self.request.POST:
            controls = self.request.POST.items()
            try:
                data = myform.validate(controls)
            except ValidationFailure, e:
                return {'form':e.render(), 'values': False, 'title':"error", 'lab': lab}
            # Process the valid form data, do some work
            lab.default_deadline1 = data['default_deadline1']
            lab.default_deadline2 = data['default_deadline2']
            print data
            print "~" * 90
            DBSession().add(lab)
            self.request.flash_fire(msg="Lab updated")
            return HTTPFound(
                location=self.request.route_path('grader_lab', lab=lab.id))

        data = dict( default_deadline1 = lab.default_deadline1,
                     default_deadline2 = lab.default_deadline2 )

        return dict( form = myform.render(data), lab = lab )
