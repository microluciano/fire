import os
from datetime import datetime, date, timedelta

from pyramid.view import view_config
from pyramid.renderers import render_to_response
from pyramid.httpexceptions import HTTPNotFound, HTTPFound
from pyramid.events import subscriber, BeforeRender
from pyramid.security import remember
import deform

import sqlalchemy
from sqlalchemy import text

from .. import filestore, templating, mail, settings
from ..models import *
from ..errors import UserException, InternalFireError
from ..forms import make_form, StudentSettingsSchema, SubmissionsAdminSchema

import logging
log = logging.getLogger(__name__)

@subscriber(BeforeRender)
def add_grader_menu(event):
    request = event['request']
    if request.user and request.user.role == 'grader':
        menu = event.get('action_menu', [])
        menu.append((request.route_path('grader_roster'), 'Course roster'))
        event['action_menu'] = menu

@view_config(route_name='grader_main', 
             renderer='fire:templates/grader_main.html',
             permission='has_role:grader')
def grader_main(request):
    labs = Lab.get_active()

    labinfo = list()
    for lab in labs:
        stats = dict()
        submitted = [s for s in lab.submissions if s.status == Submission.Statuses.submitted]
        stats["own"] = len([s for s in submitted if s.grader is request.user])
        stats["submitted"] = len(submitted)
        stats["rejected"] = len([s for s in lab.submissions if s.status == Submission.Statuses.rejected])
        stats["accepted"] = len([s for s in lab.submissions if s.status == Submission.Statuses.accepted])
        stats["physical_waiting"] = len([s for s in submitted 
            if s.physical_submission and s.physical_submission_received_date is None])

        labinfo.append(dict(lab=lab, stats=stats))

    return dict(labinfo=labinfo)


@view_config(route_name='grader_roster',
             renderer='fire:templates/grader_roster.html',
             permission='has_role:grader')
def grader_roster(request):
    students = Student.get_all()
    groups = Group.get_all()
    labs = Lab.get_active()
    summaries = { (s.email, l.id): l.summary_for_user(s)
                    for s in students
                    for l in labs }
    unverified_emails = EmailVerification.get_unverified()
    return dict(
            students=students,
            labs=labs,
            summaries=summaries,
            unverified_emails = unverified_emails )


@view_config(route_name='grader_lab', 
             renderer='fire:templates/grader_lab.html',
             permission='has_role:grader')
def grader_lab(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    submitted = [s for s in lab.submissions if s.status == Submission.Statuses.submitted]
    rejected  = [s for s in lab.submissions if s.status == Submission.Statuses.rejected]
    accepted  = [s for s in lab.submissions if s.status == Submission.Statuses.accepted]

    submitted.sort(key=lambda s: s.submitted_date)
    rejected.sort(key=lambda s: s.reviewed_date)
    accepted.sort(key=lambda s: s.reviewed_date)

    return dict(lab=lab, submitted=submitted, rejected=rejected, accepted=accepted)

@view_config(route_name='grader_download_pending', 
             permission='has_role:grader')
def grader_download_pending(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    pending = [s for s in lab.submissions 
                    if s.status == Submission.Statuses.submitted]
                        #and s.grader is request.user] # TODO

    root_dir = "submissions_%s_%s" % (settings.get(request, 'course_slug'),lab.slug if lab.slug else lab.id)

    filelisting = []
    for sub in pending:
        submitter_dir = sub.submitters[0].email if lab.individual else ('group%02d' % sub.group.id)
        _collect_files_for_sub(request, filelisting, sub,
                os.path.join(root_dir, submitter_dir))
        
        # Find older submissions as well
        submitter = sub.submitters[0] if lab.individual else sub.group
        othersubs = [s for s in submitter.submissions_for_lab(lab)
                    if s.status in (Submission.Statuses.accepted,
                                    Submission.Statuses.rejected)]
        for othersub in othersubs:
            _collect_files_for_sub(request, filelisting, othersub,
                    os.path.join(root_dir, submitter_dir, "submission%02d" % othersub.number))
    
    cap = FileCap.issue_archive(filelisting, root_dir, 600)
    
    cap_url = settings.get(request, 'url_capserver') + request.route_path('get_filecap', filecap_id=cap.id, filename=cap.filename)
    
    return HTTPFound(cap_url)

def _collect_files_for_sub(request, filelisting, sub, prefix):
    subfiles = filestore.files_for_submission(request, sub)
    for f in subfiles['files']:
        filelisting.append((   # Note that this is a pair
            filestore.fullpath_for_file(request, sub, f.path),
            os.path.join(prefix, f.name) ))
    for f in subfiles['deleted_files']:
        filelisting.append((   # Note that this is a pair
            filestore.fullpath_for_file(request, sub, f['path']),
            os.path.join(prefix, '__deleted', f['name']) ))


@view_config(route_name='grader_physical_overview',
             renderer='fire:templates/grader_physical_overview.html',
             permission='has_role:grader')
def grader_physical_overview(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    if not lab.allow_physical_submissions:
        request.flash_fire(type='error', msg=('Lab %s does not allow physical submissions.' % lab.slug))
        raise HTTPFound(request.route_path('grader_lab', lab=lab.id))

    if lab.individual:
        submitters = User.get_all_students()
    else:
        submitters = Group.get_all() # TODO test

    # TODO: refactor this into User/Group models
    pending_submissions = []
    for s in submitters:
        subs = [sub for sub in s.submissions_for_lab(lab) 
                if sub.status == Submission.Statuses.submitted]
        if len(subs) > 0:
            assert len(subs) == 1  # Should be maintained by models.py
            pending_submissions.append(subs[0])
        else:
            pending_submissions.append(None)

    return dict(lab=lab, submitters = zip(submitters, pending_submissions),
            csrf_token=request.session.get_csrf_token())


@view_config(route_name='grader_register_physical',
             request_method='POST',
             renderer='json',
             check_csrf=True,
             permission='has_role:grader')
def grader_register_physical(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    if lab.individual:
        submitter = User.get(request.POST.getone('submitter_id'))
    else:
        submitter = Group.get(int(request.POST.getone('submitter_id')))

    submission = submitter.get_pending_submission(lab)
    if submission is None:
        submission = submitter.open_new_submission(lab)
        submission.physical_submission = True
    if submission is None:
        return dict(error="Couldn't find or open a new submission.")

    if submission.physical_submission_received_date is not None:
        return dict(error="Physical submission is already registered.")

    submission.physical_submission_received_by = request.user
    submission.physical_submission_received_date = datetime.now()

    # We skip the deadline check here, since it is a grader performing this op
    # TODO: warn the grader if the deaadline has passed.
    submission.status = Submission.Statuses.submitted

    submitter_name = None
    if lab.individual:
        submitter_name = submitter.name
    else:
        submitter_name = "Group %d" % submitter.id
    
    mail.sendmail(request, [u.email for u in submission.submitters], 
            'Your physical submission has been received', 
            'fire:templates/mails/notify_physical_registration.txt',
            dict(submission = submission,
                user = request.user.safe_info,
                submitter_name = submitter_name,
                course_name = Settings.get('course_name')))

    return dict(success=True)


@view_config(route_name='grader_submissions', 
             renderer='fire:templates/grader_submissions.html',
             permission='has_role:grader')
def grader_submissions(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    group_or_user = request.matchdict['group_or_user']

    if lab.individual:
        submitter = User.get_or_404(group_or_user)
    else:
        groupid = group_or_user
        if groupid.startswith('group'):
            groupid = int(groupid[5:])
        else:
            return HTTPNotFound()
        submitter = Group.get_or_404(groupid)

    submissions = [s for s in submitter.submissions_for_lab(lab) if s.status != Submission.Statuses.new]
    filesets = [filestore.files_for_submission(request, s) for s in submissions]

    last_sub_files = (None, None)
    if len(submissions):
        last_sub_files = (submissions[-1], filesets[-1])
    older_subs_files = zip(submissions[:-1], filesets[:-1])
   
    admin_form = make_form(
            SubmissionsAdminSchema().bind(
                graders=Grader.get_all()),
            buttons=('save',),
            action=request.route_path('grader_submissions_admin', 
                lab=lab.id, group_or_user=group_or_user),
            formid='adminform',
            use_ajax=True)

    activesub = submitter.open_new_submission(lab)
    admin_data = {k: getattr(activesub, k)
            for k in ('deadline1_override', 'deadline2_override', 'grader_id')}
    admin_form.set_appstruct(admin_data)
    log.debug(repr(admin_data))

    return dict(
            lab = lab,
            submitter = submitter,
            last_sub_files = last_sub_files,
            older_subs_files = older_subs_files,
            group_or_user = request.matchdict['group_or_user'],
            admin_form = admin_form,
            csrf_token=request.session.get_csrf_token()
            )

@view_config(route_name='grader_autosave_review',
             request_method='POST',
             renderer='json',
             check_csrf=True,
             permission='has_role:grader')
def grader_autosave_review(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    submission = Submission.get_or_404(int(request.POST.getone('submission_id')))

    if lab.individual:
        submitter = User.get_or_404(request.matchdict['group_or_user'])
        assert len(submission.submitters) == 1 and submission.submitters[0] is submitter
    else:
        groupid = request.matchdict['group_or_user']
        if groupid.startswith('group'):
            groupid = int(groupid[5:])
        else:
            return HTTPNotFound()
        submitter = Group.get_or_404(groupid)
        assert submission.group is submitter  # TODO better errors

    submission.grade_draft = request.POST.getone('grade')
    submission.note_draft = request.POST.getone('note')
    submission.review_draft = request.POST.getone('review')

    return dict(success=True)

@view_config(route_name='grader_post_review',
             request_method='POST',
             check_csrf=True,
             renderer='json',
             permission='has_role:grader')
def grader_post_review(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    submission = Submission.get_or_404(int(request.POST.getone('submission_id')))

    if lab.individual:
        submitter = User.get_or_404(request.matchdict['group_or_user'])
        assert len(submission.submitters) == 1 and submission.submitters[0] is submitter
    else:
        groupid = request.matchdict['group_or_user']
        if groupid.startswith('group'):
            groupid = int(groupid[5:])
        else:
            return HTTPNotFound()
        submitter = Group.get_or_404(groupid)
        assert submission.group is submitter  # TODO better errors

    accept = request.POST.getone('accept') == 'yes'
    grade = request.POST.getone('grade')
    note = request.POST.getone('note')
    review = request.POST.getone('review')

    submission.grader = request.user
    submission.post_review(accept, grade, note, review)

    subject = ('Lab %s is accepted' if accept else 'Lab %s is rejected') % lab.title

    submitter_name = None
    if lab.individual:
        submitter_name = submitter.name
    else:
        submitter_name = "Group %d" % submitter.id

    mail.sendmail(request, [u.email for u in submission.submitters], 
            subject,
            'fire:templates/mails/notify_review.txt',
            dict(submission = submission,
                user = request.user.safe_info,
                submitter_name = submitter_name,
                accepted = accept,
                review = review,
                course_name = Settings.get('course_name')))

    return HTTPFound(request.route_path('grader_lab', lab=lab.id))

@view_config(route_name='grader_download_file',
             permission='has_role:grader')
def grader_download_file(request):
    lab = Lab.get_or_404(request.matchdict['lab'])
    sub = Submission.get_or_404(request.matchdict['submission'])
    assert sub.lab is lab

    if lab.individual:
        submitter = User.get_or_404(request.matchdict['group_or_user'])
        assert len(sub.submitters) == 1 and sub.submitters[0] is submitter
    else:
        groupid = request.matchdict['group_or_user']
        if groupid.startswith('group'):
            groupid = int(groupid[5:])
        else:
            return HTTPNotFound()
        submitter = Group.get_or_404(groupid)
        assert sub.group is submitter

    filename = request.matchdict['filename']

    fs = filestore.files_for_submission(request, sub)
    fileinfo = next((f for f in fs['files'
        ] if f.name == filename), None)
    if fileinfo is None:
        return HTTPNotFound()

    fullpath = filestore.fullpath_for_file(request, sub, filename)
    cap = FileCap.issue_single(fullpath, filename)

    cap_url = settings.get(request, 'url_capserver') + request.route_path('get_filecap', filecap_id=cap.id, filename=cap.filename)

    dl = len(request.params.getall("dl")) > 0
    if dl:
        cap_url = cap_url + "?dl=1"
    return HTTPFound(cap_url)

@view_config(route_name='grader_submissions_admin',
             request_method='POST', check_csrf=True,
             renderer='genshistream',
             permission='has_role:grader')
def grader_submissions_admin(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    group_or_user = request.matchdict['group_or_user']

    if lab.individual:
        submitter = User.get_or_404(group_or_user)
    else:
        groupid = group_or_user
        if groupid.startswith('group'):
            groupid = int(groupid[5:])
        else:
            return HTTPNotFound()
        submitter = Group.get_or_404(groupid)

    form = make_form(
            SubmissionsAdminSchema().bind(
                graders = Grader.get_all()),
            buttons=('save',),
            action=request.route_path('grader_submissions_admin', 
                lab=lab.id, group_or_user=group_or_user),
            formid='adminform',
            use_ajax=True)
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return e.render()

    sub = submitter.open_new_submission(lab)
    sub.deadline1_override = data['deadline1_override']
    sub.deadline2_override = data['deadline2_override']
    sub.grader = User.get(data['grader_id'])

    newdata = dict( deadline1_override = sub.deadline1_override,
                    deadline2_override = sub.deadline2_override,
                    grader_id = sub.grader.email )
    return form.render(newdata, successmsg='Saved')


@view_config(route_name='grader_impersonate',
        request_method='POST', check_csrf=True,
        permission='impersonate')
def grader_impersonate_activate(context, request):
    user = context
    headers = remember(request, user.email)
    request.session.invalidate()
    request.flash_fire(type='success', msg='You are now logged in as %s. Remember to logout when done.' % user.email)
    return HTTPFound(location=request.route_path('root'), headers=headers)

@view_config(route_name='grader_view_student',
        renderer='fire:templates/grader_view_student.html',
        permission='view')
def grader_view_student(context, request):
    course_codes = [cc.code for cc in CourseCode.get_all()]
    form = make_form(StudentSettingsSchema().bind( course_codes = course_codes),
            buttons=('save',),
            action=request.route_path('grader_edit_student', student_id=context.id),
            formid='settingsform', 
            use_ajax=True)
    data = { k: getattr(context, k)
             for k in ('first_name', 'last_name', 'id_number', 'course') }
    form.set_appstruct(data)
    return dict(form=form, student=context, 
            csrf_token=request.session.get_csrf_token())

@view_config(route_name='grader_edit_student',
        renderer='genshistream',
        request_method='POST', check_csrf=True,
        permission='edit')
def grader_edit_student(context, request):
    course_codes = [cc.code for cc in CourseCode.get_all()]
    form = make_form(StudentSettingsSchema().bind( course_codes = course_codes),
            buttons=('save',),
            action=request.route_path('update_settings'),
            formid='settingsform', 
            use_ajax=True)
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return e.render()
    
    # Update the user
    u = context
    u.first_name = data['first_name']
    u.last_name  = data['last_name']
    u.id_number  = data['id_number']
    u.course     = data['course']

    data = { k: getattr(context, k)
             for k in ('first_name', 'last_name', 'id_number', 'course') }

    return form.render(data, successmsg='Saved!')

@view_config(route_name='graph_submissions',
             request_method='GET',
             renderer='json',
             permission='has_role:grader')
def graph_submissions(request):
    """
    This view returns a json list of the number of submission per day for the
    last 42 days. It is used to build the github-style bar chart in the lab
    overview page.

    Note that we need to write a sql query manually here because
    sqlalchemy doesn't let us use the DATE function of sqlite.

    We do two query, one for the number of submission assigned to the
    current grader and one for the total number.
    """

    conn = DBSession.get_bind()

    lab_id = int(request.matchdict['lab'])
    grader_id = request.user.email
    today = date.today()

    data = dict( all = [], me = [])

    query = text("""
      SELECT DATE(submitted_date), COUNT(*)
      FROM Submissions
      WHERE DATE(submitted_date) >= DATE(julianday(date('now'))-41)
      AND lab_id = :lab_id
      GROUP BY DATE(submitted_date)
    """)
    dbdata = dict(list( conn.execute( query, lab_id = lab_id ) ) )
    for i in range(0,42):
      d = today - timedelta(days=i)
      data['all'].append(dbdata.get(str(d),0))

    query = text("""
      SELECT DATE(submitted_date), COUNT(*)
      FROM Submissions
      WHERE DATE(submitted_date) >= DATE(julianday(date('now'))-41)
      AND lab_id = :lab_id
      AND grader_id = :grader_id
      GROUP BY DATE(submitted_date)
    """)
    dbdata = dict(list( conn.execute( query, lab_id = lab_id, grader_id = grader_id ) ) )
    for i in range(0,42):
      d = today - timedelta(days=i)
      data['me'].append(dbdata.get(str(d),0))

    return data
