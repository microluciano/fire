from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound
from deform import Form, ValidationFailure

from fire.models import *
from ..schemata import RegisterSchema, StudentCreateAccountSchema
from ..forms import make_form_message, make_form
from .. import mail, settings

@view_defaults(
        route_name = 'signup',
        permission='__no_permission_required__')
class SignupView(object):
    """
    Views to allow students to signup. Signup is divided in the following steps:
    - the student start by entering her email
    - a validation token is generated (by the EmailVerification class) and
      send to the entered email address.
    - when clicking on the validation link, the student is presented a form
      where she enter information (name, personnummer, course code...)
    - the account is created and the student can now log in.
    """

    def __init__(self, request):
        self.request = request
        course_codes = [cc.code for cc in CourseCode.get_all()]
        self.signupform = make_form(
                StudentCreateAccountSchema().bind(
                      course_codes = course_codes ),
                buttons=('register',),
                action=self.request.route_path('signup'),
                formid='signupform' )

    @view_config(
            renderer='genshistream',
            request_method='POST',
            request_param = 'signup_email',
            check_csrf=True)
    def step1_email(self):
        if self.request.user:
            self.request.flash_fire(
                    type='error',
                    msg="Please log out if you want to register a new student." )
            return HTTPFound( location=request.route_path('login') )

        signupform = make_form(RegisterSchema(),
                buttons=('register',),
                action=self.request.route_path('signup'),
                formid='signupform',
                use_ajax=True)
        try:
            data = signupform.validate( self.request.POST.items() )
        except ValidationFailure, e:
            # TODO: ValidationFailure.render does not accept the same arguments as
            #       Form.render does. So we don't get the nice form on error.
            #return e.render(css_class='span4 well', show_labels=False)
            return e.render()

        verification = EmailVerification.create( email = data['signup_email'] )

        mail.sendmail(self.request, [data['signup_email']],
                'Please verify your Fire account',
                'fire:templates/mails/verify_email.txt',
                dict( url = self.request.route_url(
                        'signup', _query={'token':verification.token}),
                      course_name = Settings.get('course_name')))

        return make_form_message(
                type='success',
                msg='Please visit verification URL sent by email.',
                css_class="span4", formid="signupform")


    @view_config(
            renderer='fire:templates/signup.html',
            request_method='GET',
            request_param = 'token',
            check_csrf=False)
    def step2_verification(self):
        token = self.request.params['token']
        try:
            email = EmailVerification.check(token)
            if User.get(email) is None:
                self.request.session['email'] = email
                return dict( email = email, form = self.signupform )
            else:
                return dict( error = 'Email already validated' )
        except ValueError, e:
            return dict( error = e.message )

    @view_config(
            renderer='fire:templates/signup.html',
            request_method = 'POST',
            request_param = 'first_name',
            check_csrf = True)
    def step3_account(self):
        email = self.request.session['email']
        try:
            data = self.signupform.validate( self.request.POST.items() )
            print data
        except ValidationFailure, e:
            return dict( form = e )

        s = Student.create(email,
                first_name = data['first_name'],
                last_name = data['last_name'],
                password = data['password'],
                id_number = data['id_number'],
                course = data['course'] )

        self.request.flash_fire(
                msg="Acount created. You can now login." )
        return HTTPFound( location=self.request.route_path('root') )
