import os
import re
import tarfile, tempfile
from datetime import datetime

from pyramid.view import view_config
from pyramid.security import remember, forget, Authenticated
from pyramid.httpexceptions import HTTPFound, HTTPGone, HTTPForbidden, HTTPNotFound
from pyramid.response import FileResponse, FileIter, Response
import deform

from .. import settings, mail, errors
from ..models import User, Student, Settings, DBSession, FileCap, CourseCode
from ..security import random_hex_string
from ..forms import (make_form, 
                     make_form_message, 
                     RegisterSchema,
                     StudentSettingsSchema,
                     GraderSettingsSchema,
                     PasswordSchema)

import logging
log = logging.getLogger(__name__)

@view_config(route_name='root', permission='__no_permission_required__')
def root(request):
    return HTTPFound(location=request.route_path('login'))

@view_config(route_name='login', renderer='fire:templates/login.html', 
        permission='__no_permission_required__')
def login(request):
    post_data = request.POST
    log.debug('start login: %r', post_data)
    if 'email' in post_data:
        log.debug('Attempting login')
        email = post_data['email']       # will be used as the user id, not necessarily email
        passwd = post_data['password']
        if User.check_password(email, passwd):
            u = User.get(email)
            u.last_login = datetime.now()
            log.debug('logged in, setting headers')
            if 'remember' in post_data:
                headers = remember(request, email, max_age=settings.get(request, 'login_lifetime'))
            else:
                headers = remember(request, email)
            request.session.invalidate()
            return HTTPFound(location=request.route_path('root'), headers=headers)
        else:
            log.debug('login failed')
            headers = forget(request)
            request.session.invalidate()
            request.flash_fire(type='error', msg='Incorrect user name (email) or password. Contact the teachers if you forgot your password.')
            return HTTPFound(location=request.route_path('login'), headers=headers)

    # We use the chance here and issue a *new* fresh csrf token.
    # Open windows on this session will fail anyways since a login
    # success or failure both invalidate the current session.
    request.session.new_csrf_token()

    signupform = make_form(RegisterSchema(), 
                            buttons=('register',),
                            action=request.route_path('signup'),
                            formid='signupform', 
                            use_ajax=True)
    return dict(signupform=signupform)

@view_config(route_name='logout', permission='__no_permission_required__')
def logout(request):
    headers = forget(request)
    request.session.invalidate()
    return HTTPFound(location=request.route_path('login'), headers=headers)


@view_config(route_name='get_filecap', permission='__no_permission_required__')
def get_filecap(request):
    if request.user is not None and not settings.get(request, 'debug'):
        return HTTPForbidden('Filecaps cannot be opened under user credentials (server misconfiguration).')
    fc = FileCap.get_or_404(request.matchdict['filecap_id'])
    filename = request.matchdict['filename']
    if filename != fc.filename:
        return HTTPNotFound()
    if fc.expires and fc.expires < datetime.now():
        return HTTPGone()  # 410
    dl = len(request.params.getall("dl")) > 0
    # TODO store and check ip address
    if fc.is_archive:
        # Consider switching this to NamedTemporaryFile and avoid useing FileIter
        tmpfile = tempfile.TemporaryFile()
        archive = tarfile.open(mode='w:gz', fileobj=tmpfile)
        for fullpath, archivepath in fc.contents:
            assert os.path.isfile(fullpath)
            archive.add(fullpath, archivepath)
        archive.close()
        size = tmpfile.tell()
        tmpfile.seek(0)
        resp = Response(content_type='application/x-gzip')
        resp.headers['Content-Disposition'] = "attachment; filename=%s.tar.gz" % str(fc.filename)
        resp.headers['Content-Length'] = str(size)
        resp.app_iter = FileIter(tmpfile)
        return resp
    else:
        resp = FileResponse(fc.contents, request)
        if dl:
            # Don't specify a filename, as encoding it correctly here is fubar.
            # Instead, the browser will guess a correct filename from the url.
            resp.headers['Content-Disposition'] = "attachment"
        return resp

@view_config(route_name='user_settings', 
        renderer='fire:templates/generic_form.html',
        permission='is:authenticated')
def user_settings(request):
    if request.user.role == 'student':
        course_codes = [cc.code for cc in CourseCode.get_all()]
        schema = StudentSettingsSchema().bind( course_codes = course_codes)
    else:
        schema = GraderSettingsSchema()
    form = make_form(schema,
            buttons=('save',),
            action=request.route_path('update_settings'),
            formid='settingsform', 
            use_ajax=True)
    data = { k: getattr(request.user, k)
             for k in ('first_name', 'last_name', 'id_number', 'course')
             if hasattr(request.user, k) }

    return dict(title='User settings',
                for_line=request.user.name, 
                form=form.render(data))

@view_config(route_name='update_settings',
        renderer='genshistream',
        request_method='POST', check_csrf=True,
        permission='is:authenticated')
def update_settings(request):
    if request.user.role == 'student':
        course_codes = [cc.code for cc in CourseCode.get_all()]
        schema = StudentSettingsSchema().bind( course_codes = course_codes)
    else:
        schema = GraderSettingsSchema()
    form = make_form(schema,
            buttons=('save',),
            action=request.route_path('update_settings'),
            formid='settingsform', 
            use_ajax=True)
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return e.render()
    
    # Update the user
    for k in ('first_name', 'last_name', 'id_number', 'course'):
        if k in data:
            setattr(request.user, k, data[k])

    data = { k: getattr(request.user, k)
             for k in ('first_name', 'last_name', 'id_number', 'course')
             if hasattr(request.user, k)}

    return form.render(data, successmsg='Saved!')

@view_config(route_name='user_password', 
        renderer='fire:templates/generic_form.html',
        permission='is:authenticated')
def user_password(request):
    form = make_form(PasswordSchema(), 
            buttons=('save',),
            action=request.route_path('update_password'),
            formid='passwordform') 
    return dict(title="Change password", form=form.render(errormsg="Note: You will need to log in again with the new password."))

@view_config(route_name='update_password',
        renderer='fire:templates/generic_form.html',
        request_method='POST', check_csrf=True,
        permission='is:authenticated')
def update_password(request):
    form = make_form(PasswordSchema(), 
            buttons=('save',),
            action=request.route_path('update_password'),
            formid='passwordform')
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return dict(title="Change password", form=e.render())

    # Change the password and then log out
    request.user.password = data['password']
    r = logout(request)
    request.flash_fire(msg="Your password has been updated, please log in with the new password.")
    return r

