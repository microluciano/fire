from random import choice
from string import ascii_letters

from pyramid.view import view_config, view_defaults
from fire.models import DBSession, Grader, Lab, Settings
from pyramid.httpexceptions import HTTPFound

from .. import mail


@view_defaults( route_name = 'admin_graders', permission='has_role:grader')
class AdminGradersView(object):
    """
    This view takes care of operation on the set of graders.
    This include displaying, adding and removing graders.
    """

    def __init__(self, request):
        self.request = request

    @view_config( renderer = 'fire:templates/admin_graders.html',
                  request_method="GET" )
    def list_view(self):
        graders = Grader.get_all()
        labs = Lab.get_all()
        return dict( graders = graders, labs = labs )

    @view_config( request_method='POST', request_param='newgrader', check_csrf=True )
    def add_grader(self):
        newgrader = self.request.params['newgrader']
        if newgrader:
            grader = Grader(newgrader)
            password = ''.join(choice(ascii_letters) for _ in xrange(12))
            grader.password = password
            DBSession.add(grader)
            mail.sendmail(
                self.request, [grader.id], 'Your password',
                'fire:templates/mails/new_grader.txt',
                { 'name': grader.name,
                  'course': Settings.get('course_name'),
                  'password': password,
                  'url': self.request.route_url('root')
                })
        return HTTPFound(self.request.route_path('admin_graders'))
