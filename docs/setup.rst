Setup and system components
***************************

This document describes the prerequisites on a Fire server. 
Fire setup is divided in two parts, *course setup* which specifies a particular
release version of Fire and associated data files, and the *system setup* which
includes server requirements and the setup of a main webserver that distributes
requests to individual course instances.

Course setup
------------

.. todo:: write this

Stating a vew course
....................

The web server is nginx. However, each fire instance runs as a fastcgi python
server, each under a separate user. The nginx config is under /etc/nginx. The
fire.conf file is for the old version of fire, so just ignore that. In
nginx.conf there are two places to change, look for "fafl". You can see in two
places very similar regexes that match paths like /(?<course>lbs|fafl|...).../.
They proxy requests to the right socket file for that instance, stored under
/var/run/fire/coursename.sock. Add your course slug to the two regex.

Then you need to create a user called fire_X where X is the course name::

    useradd -m -r fire_X

Under that user, install virtualenv and use it
to create a virtual env directory called .fire-virtualenv in the home
directory. In it, install fire and its dependencies with pip::

    sudo -u fire_X -i
    virtualenv -p python2.7 ~/.fire-virtualenv
    . ~/.fire-virtualenv/bin/activate
    pip install /home/arnar/chalmers/fire/pyramid_repo/dist/fire-2.0a8.tar.gz

Then create a fire.conf file in the home directory, again look at fire_fafl for
the example. Create also a directory for the data, e.g. data-2014 or whatever
path you use in fire.conf and for the logs::


    mkdir ~/data-2014 ~/logs

Make sure the virtualenv is active and run "fireadmin -c fire.conf initdb".
This will populate the data directory and create the sqlite file::


    fireadmin -c fire.conf initdb

Start the new fire instance::

    systemctl start fire@cp

This should start the python server and create the socket file. If you want it
to start when the server is restarted, you need to enable it::

    systemctl enable fire@cp

If you didn't already, reload the nginx config (systemctl reload nginx, or
restart if reload doesn't work). Now you should be able to visit
http://xdat09.ce.chalmers.se/X/ and see your new instance.To create users etc,
for now, log in as the user, activate the virtualenv and run pshell. Then you
have a python shell where you can do stuff like::

    import transaction
    from fire.models import *
    with transaction.manager:
      u = User('someone@somewhere.com')
      u.password = 't0ps3cr3t'
      u.role = User.Roles.admin
      DBSession().add(u)
      # ⏎ ⏎ to close the with block

This should commit the transaction if there are no errors. Otherwise,
refer to the docs on SQLAlchemy and the transaction package.

Setup crontab, that is used to send mail asynchronously. Run ``crontab -e``.
Sample config::

    * * * * * ~/.fire-virtualenv/bin/qp --hostname mail.medic.chalmers.se ~/XXXX/mailq 1> ~/logs/mail.log 2>&1

where ``XXXX`` is a directory with data (the same as ``fire.data_dir`` in ``fire.conf``).
To make sure, that changes went though, run ``crontab -l``.

TODO: nginx config

System setup
------------

.. todo:: write this
